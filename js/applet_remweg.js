/**
 *
 * Copyleft 2018, 2021 Bartavi (Bart Groeneveld)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

function Configuration() {
	this.initialVelocity = 10 // v (m/s)
	this.stepBase = 60 // steps/t (steps/s)
	this.horizontalMetersInBackgroundTile = 12 // s (m)
	this.vehicleMass = 1000 // m (kg)
	this.brakePower = 10000 // F (N)
	this.roadFriction = 1.0 // factor
	this.requestStopBetween = { min: 1.0, max: 2.0 } // t (s)
	this.requestedInitialDistanceToCollisionObject = 100 // s (m)
	this.autoBrakeAfter = 1.9 // t (s)
	this.expectedReactionTime = 2.0 // t (s)
	this.setSafeDistanceToCollisionObject = true
	this.vehicleWidth = 4 // s (m)
	this.paddingLeft = 1.5 // vehicle lengths from front of vehicle
	this.paddingRight = 0.5 // vehicle lengths
}

function World() {
	this.runID = null
	this.currentStep = 0
	this.manuallyBrakedOnStep = null
}

// eslint-disable-next-line no-unused-vars
const app = new Vue({
	el: '#app',
	filters: {
		formatInt: function(v) {
			const int = new Intl.NumberFormat(undefined, {
				minimumSignificantDigits: 4,
				maximumSignificantDigits: 4
			})
			return int.format(v)
		}
	},
	data: {
		configuration: new Configuration(),
		world: new World(),
		configurationIsReadOnly: false
	},
	computed: {
		velocityAsKMH: {
			get: function() {
				return this.configuration.initialVelocity * 3.6
			},
			set: function(nV) {
				this.configuration.initialVelocity = nV / 3.6
			}
		},

		effectiveBrakeForce: function() {
			return this.configuration.brakePower * this.configuration.roadFriction // F (N)
		},
		effectiveDeceleration: function() {
			return this.effectiveBrakeForce / this.configuration.vehicleMass // a (m/s^2)
		},
		timeTillStop: function() {
			return this.configuration.initialVelocity / this.effectiveDeceleration // t (s)
		},
		stepsTillStop: function() {
			return this.timeTillStop * this.configuration.stepBase // steps
		},
		totalBrakeDistance: function() {
			return prm.decelerationDistance(this.configuration.initialVelocity,
				this.effectiveDeceleration, this.timeTillStop) // s (m)
		},
		safeInitialDistanceToCollisionObject: function() {
			return this.configuration.expectedReactionTime * this.configuration.initialVelocity
				+ this.totalBrakeDistance // s (m)
		},
		actualInitialDistanceToCollisionObject: function() {
			return this.configuration.setSafeDistanceToCollisionObject
				? this.safeInitialDistanceToCollisionObject
				: this.configuration.requestedInitialDistanceToCollisionObject // s (m)
		},
		worldLength: function() {
			const totalRequiredLength = this.configuration.paddingLeft * this.configuration.vehicleWidth
				+ this.actualInitialDistanceToCollisionObject
				+ this.configuration.vehicleWidth // collision object width
				+ this.configuration.paddingRight * this.configuration.vehicleWidth
			return totalRequiredLength
		},

		currentTime: function() {
			return this.world.currentStep / this.configuration.stepBase // t (s)
		},
		requestStopOnTime: function() {
			return prm.getRandomArbitrary(this.configuration.requestStopBetween) // t (s)
		},
		requestStopOnStep: function() {
			return this.requestStopOnTime * this.configuration.stepBase // step
		},
		stopIsRequested: function() {
			return (this.world.currentStep >= this.requestStopOnStep)
		},
		requestedStopOnStep: function() {
			return Math.min(this.requestStopOnStep, this.world.currentStep)
		},

		manualBrakeActivated: function() {
			return this.world.manuallyBrakedOnStep != null
		},
		autoBrakeAfterStepsFromRequest: function() {
			return this.configuration.autoBrakeAfter * this.configuration.stepBase // steps
		},
		autoBrakeOnStep: function() {
			return this.requestStopOnStep + this.autoBrakeAfterStepsFromRequest // steps
		},
		autoBrakeActivated: function() {
			return this.world.currentStep >= this.autoBrakeOnStep
				// Only auto brake if not yet manually braked
				&& (!this.manualBrakeActivated
					|| this.world.manuallyBrakedOnStep > this.autoBrakeOnStep)
		},
		brakedAfterStepsFromRequest: function() {
			if (this.autoBrakeActivated) return this.autoBrakeAfterStepsFromRequest
			else if (this.manualBrakeActivated) return this.world.manuallyBrakedOnStep - this.requestedStopOnStep
			else return this.world.currentStep - this.requestedStopOnStep
		},
		anyBrakeActivated: function() {
			return this.manualBrakeActivated || this.autoBrakeActivated
		},
		brakedOnStep: function() {
			return this.requestedStopOnStep + this.brakedAfterStepsFromRequest
		},
		stepsSinceBraking: function() {
			return this.world.currentStep - this.brakedOnStep
		},
		brakedAfterTimeFromRequest: function() {
			return this.brakedAfterStepsFromRequest / this.configuration.stepBase // t (s)
		},
		brakedAfterDistance: function() {
			return this.brakedAfterTimeFromRequest * this.configuration.initialVelocity // s (m)
		},
		currentBrakeStep: function() {
			// Prevent reversing after full stop
			return Math.min(this.stepsSinceBraking, this.stepsTillStop)
		},
		currentBrakeTime: function() {
			return this.currentBrakeStep / this.configuration.stepBase
		},
		currentBrakeDistance: function() {
			return prm.decelerationDistance(this.configuration.initialVelocity,
				this.effectiveDeceleration, this.currentBrakeTime) // s (m)
		},
		currentVelocity: function() {
			return prm.velocityAfterBraking(this.configuration.initialVelocity,
				this.effectiveDeceleration, this.currentBrakeTime) // s (m)
		},

		vehiclePosBeforeRequestToStop: function() {
			const steps = this.requestedStopOnStep
			const time = steps / this.configuration.stepBase
			return time * this.configuration.initialVelocity
		},
		vehiclePosAfterRequestToStop: function() {
			return this.brakedAfterDistance + this.currentBrakeDistance
		},

		brakingCompleted: function() {
			return this.currentBrakeStep === this.stepsTillStop
		},
		collided: function() {
			return this.vehiclePosAfterRequestToStop >= this.actualInitialDistanceToCollisionObject
		},
		stopped: function() {
			return this.brakingCompleted || this.collided
		},

		pixelsPerMeter: function() {
			const absoluteWorldLength = $('#world').width() // px
			return absoluteWorldLength / this.worldLength // px/s (px/m)
		},
		worldStyle: function() {
			const horizontalBackgroundTilePixels = this.pixelsPerMeter
				* this.configuration.horizontalMetersInBackgroundTile // px

			const vehiclePosPixels = this.pixelsPerMeter * this.vehiclePosBeforeRequestToStop // px
			const backgroundPos = -1 * (vehiclePosPixels % horizontalBackgroundTilePixels)

			return {
				backgroundSize: horizontalBackgroundTilePixels + 'px 100%',
				backgroundPositionX: backgroundPos + 'px'
			}
		},
		vehicleWidthPixels: function() {
			return this.configuration.vehicleWidth * this.pixelsPerMeter
		},
		displayOffset: function() {
			return this.configuration.paddingLeft * this.vehicleWidthPixels
		},
		vehicleLeft: function() {
			const pos = this.pixelsPerMeter * this.vehiclePosAfterRequestToStop
			return pos + this.displayOffset
		},
		brakeLeft: function() {
			const pos = this.pixelsPerMeter * this.brakedAfterDistance
			return pos + this.displayOffset
		},
		hypotheticalStopLeft: function() {
			const pos = this.pixelsPerMeter * (this.totalBrakeDistance + this.brakedAfterDistance)
			return pos + this.displayOffset
		},
		collisionObjectLeft: function() {
			return this.pixelsPerMeter * this.actualInitialDistanceToCollisionObject
				+ this.displayOffset
		},
		collisionObjectColor: function() {
			if (this.stopIsRequested) return 'firebrick'
			else return 'seagreen'
		}
	},
	watch: {
		stopped: function(v, oV) {
			this.pauseWorld()
		}
	},
	mounted: function() {
		this.$nextTick(function() {
			prm.addShortcutToButton($('#play-pause button'), ['right', 'down'], true)
			prm.addShortcutToButton($('#brake button'), ['left', 'spacebar'], true)
			prm.addShortcutToButton($('#reset button'), ['escape', 'up', 'b', 'z', 'w'], true)
		})
	},
	methods: {
		startWorld: function() {
			this.configurationIsReadOnly = true
			if (this.world.runID == null) {
				this.world.runID = setInterval(function(instance) {
					instance.nextStep()
				}, 1000 / this.configuration.stepBase, this)
			}
		},
		pauseWorld: function() {
			if (this.world.runID != null) {
				clearInterval(this.world.runID)
			}
			this.world.runID = null
		},
		resumeWorld: function() {
			if (this.stopped) return
			this.startWorld()
		},
		resetWorld: function() {
			this.pauseWorld()
			this.configurationIsReadOnly = false
			this.world = new World(this.configuration)
		},
		nextStep: function() {
			this.world.currentStep++
		},
		brake: function() {
			if (!this.stopIsRequested) return
			this.world.manuallyBrakedOnStep = this.world.currentStep
		}
	}
})
