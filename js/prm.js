/**
 *
 * Copyleft 2018, 2021 Bartavi (Bart Groeneveld)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

// Requires jQuery, vue.js and KeyboardJS

Vue.component('prm-vehicle', {
	props: {
		width: { type: Number, default: 1 },
		left: { type: Number, default: 0 },
		leftIsFront: { type: Boolean, default: true },
		color: { type: String, default: '#000080' }
	},
	data: function() {
		return {
			vehicleModelWidthHeightRatio: 1,
			loaded: false,
			svg: undefined
		}
	},
	computed: {
		vehicleModelStyle: function() {
			return {
				border: 0,
				width: this.width + 'px',
				height: this.width / this.vehicleModelWidthHeightRatio + 'px',
				left: this.left + 'px',
				marginLeft: (this.leftIsFront ? -1 : 0) * this.width + 'px'
			}
		}
	},
	watch: {
		color: {
			handler: function(v, oV) {
				if (!this.loaded) return
				this.svg.find('#car-body').css('fill', v)
			},
			immediate: true
		}
	},
	methods: {
		vehicleModelLoaded: function() {
			this.svg = $(this.$el).contents().find('svg')
			this.loaded = true
			const viewBox = this.svg.prop('viewBox')
			const dim = viewBox.baseVal
			const width = dim.width
			const height = dim.height
			this.vehicleModelWidthHeightRatio = width / height
			this.svg.find('#car-body').css('fill', this.color)
		}
	},
	template: '<iframe src="img/vehicle.svg" \
			@load="vehicleModelLoaded" class="vehicle" :style="vehicleModelStyle"></iframe>'
})

function prm() {

}

prm.getRandomArbitrary = function(range) {
	return Math.random() * (range.max - range.min) + range.min
}

prm.velocityAfterBraking = function(originalVelocity, decelaration, time) {
	return originalVelocity - decelaration * time // s (m)
}

prm.decelerationDistance = function(originalVelocity, decelaration, time) {
	const unalteredDistance = originalVelocity * time // s (m)
	const reverseDistance = (1 / 2) * decelaration * time * time // s (m)
	return unalteredDistance - reverseDistance // s (m)
}

prm.addShortcutToButton = function(el, keyCombos, enableHints) {
	keyboardJS.bind(keyCombos, function(e) {
		const buttonEl = el.filter(':visible')
		buttonEl.focus()
		buttonEl.click()
		buttonEl.blur()
	}, null, true)

	if (enableHints) {
		el.append(this.generateKeyHintsHtml(keyCombos))
	}
}

prm.generateKeyHintsHtml = function(keyCombos) {
	const keyCombosHtml = $('<kbd></kbd>').addClass('keyHints')
	keyCombos.forEach(function(keyCombo, index, array) {
		const keyComboParts = keyCombo.split(' ')
		const keyComboPartsHtml = $('<kbd></kbd>').addClass('keyCombo')
		keyComboParts.forEach(function(keyComboPart) {
			if (keyComboPart === '+') keyComboPartsHtml.append(' + ')
			else if (keyComboPart === '>') keyComboPartsHtml.append(' , ')
			else {
				const key = $('<kbd></kbd>').addClass('key')
				if (keyComboPart.length === 1) key.addClass('k-inline').append(keyComboPart.toUpperCase())
				else key.addClass('k-' + keyComboPart)
				keyComboPartsHtml.append(key)
			}
		})
		if (index + 1 < array.length) {
			keyComboPartsHtml.append(' / ')
		}
		keyCombosHtml.append(keyComboPartsHtml)
	})
	return keyCombosHtml
}
